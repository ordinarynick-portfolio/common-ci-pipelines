# GitLab CI Jobs for Linting Files

This folder contains GitLab CI jobs for linting various types of files, including shell scripts, GitLab pipeline files,
and YAML files. These jobs help ensure code quality and consistency by automatically checking for syntax and formatting
issues.

## List of Files

- **[`base_yaml_lint.yaml`](base_yaml_lint.yaml)**: Base CI job for linting YAML files, containing common
  configurations. (All jobs are hidden.)
- **[`gitlab-ci_file_lint.yaml`](gitlab-ci_file_lint.yaml)**: Job for linting the main GitLab CI pipeline file.
- **[`shell_lint.yaml`](shell_lint.yaml)**: Job for linting shell scripts.
- **[`yaml_lint.yaml`](yaml_lint.yaml)**: Job for linting YAML files.

Each file is designed to be included in your GitLab CI configuration, promoting automated code quality checks and best
practices.
