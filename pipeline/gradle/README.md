## GitLab CI Jobs for Gradle Service or Library

This folder contains GitLab CI jobs for building and checking Gradle services and libraries. These jobs are designed to
streamline the CI/CD process for Gradle projects by providing reusable and modular configurations.

### List of Files

- **[`base_gradle_job.yaml`](base_gradle_job.yaml)**: Defines a base Gradle job with common configurations for various
  Gradle tasks.
- **[`service_build_and_check.yaml`](service_build_and_check.yaml)**: Defines a job for building and checking a Gradle
  project, ensuring it meets quality standards before deployment.

Each file is intended to be included in your GitLab CI configuration, promoting consistency and efficiency in managing
Gradle-based projects.
