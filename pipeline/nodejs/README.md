## GitLab CI Jobs for NodeJS Service or Library

This folder contains GitLab CI jobs for building and checking NodeJS service or library projects. These jobs streamline
the CI/CD process for NodeJS projects by providing reusable and modular configurations.

### List of Files

- **[`base_node_job.yaml`](base_node_job.yaml)**: Base NodeJS job with common configurations for various tasks. (All
  jobs are hidden.)
- **[`service_build_and_check.yaml`](service_build_and_check.yaml)**: NodeJS service CI jobs, including linting and
  testing steps.

Each file is intended to be included in your GitLab CI configuration, promoting consistency and efficiency in managing
NodeJS-based projects.
