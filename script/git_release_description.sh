#!/bin/bash

git fetch --tags
git --no-pager log origin/"$CI_DEFAULT_BRANCH"..HEAD --pretty=tformat:"- %s"
