#!/bin/bash 
###################################################################
# Script Name	: find_next_version.sh
# Description	: Finds next git version in semantic versioning.
# Args          : 
#                 -b|--branch-name - branch name for which should next version get
#                 -n|--next - if next version or last version should be returned
#                 -p|--tag-prefix - prefix before tag (for versions in style v1.0.0)
#                 -v|--version - to override version as latest git tag
###################################################################

# parse script arguments
POSITIONAL=()
while [[ $# -gt 0 ]]; do
  key="$1"
  case $key in
    -b|--branch-name)
    BRANCH_NAME="$2"
    shift # past argument
    shift # past value
    ;;
    -n|--next)
    NEXT_VERSION="true"
    shift # past argument
    ;;
    -p|--tag-prefix)
    TAG_PREFIX="$2"
    shift # past argument
    shift # past value
    ;;
    -v|--version)
    VERSION="$2"
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
  esac
done

# Find what version will be used.
function last_version() {
    # If tag prefix is empty match only our tagging versions.
    if [[ -z "$TAG_PREFIX" ]]; then
        TAG_PREFIX="[0-9.]*"
    fi

    # Get latest tag version from commits.
    git fetch --tags
    PREFIXED_VERSION=$(git describe --abbrev=0 --tags --match "$TAG_PREFIX*" 2>/dev/null || echo '0.0.0')
    VERSION=${PREFIXED_VERSION#"$TAG_PREFIX"}
}

# Find what version will be used.
function next_version() {
    VERSION=$1

    if [[ -z "$BRANCH_NAME" ]]; then
         echo "No argument given. Please give a branch name as an argument!"
         exit 1
    fi

    # Parse version to individual values
    VNUM1=$(echo "${VERSION}" | cut -d'.' -f1)
    VNUM2=$(echo "${VERSION}" | cut -d'.' -f2)
    VNUM3=$(echo "${VERSION}" | cut -d'.' -f3)

    if [[ -z "$VNUM1" ]]; then
        VNUM1=0
    fi

    if [[ -z "$VNUM2" ]]; then
        VNUM2=0
    fi

    if [[ -z "$VNUM3" ]]; then
        VNUM3=0
    fi

    if [[ ${BRANCH_NAME} == fix/* ]]; then
      VNUM3=$((VNUM3+1))
    else # feature or anything else
      VNUM2=$((VNUM2+1))
      VNUM3=0
    fi

    VERSION="$VNUM1.$VNUM2.$VNUM3"
}
#-----------------------------------------------------------------------------------------------------------------------
# Main script function.

# If --version parameter is given then that one is used without any `question`.
if [[ -n "$VERSION" ]]; then
    echo "${VERSION}"
    exit 0
fi

# Get last version.
last_version

# Find next what version will be used
if [[ ${NEXT_VERSION} == true ]] ; then
  next_version "$VERSION"
fi

echo "$VERSION"
