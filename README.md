# CI Common

[![Pipeline Status](https://gitlab.com/ordinarynick-portfolio/infrastructure/common-ci-pipelines/badges/master/pipeline.svg)](https://gitlab.com/ordinarynick-portfolio/infrastructure/common-ci-pipelines/-/commits/master)
[![Latest Release](https://gitlab.com/ordinarynick-portfolio/infrastructure/common-ci-pipelines/-/badges/release.svg)](https://gitlab.com/ordinarynick-portfolio/infrastructure/common-ci-pipelines/-/releases)

This repository contains common GitLab Continuous Integration (CI) pipeline files designed for simplicity and ease of
use. Each pipeline file is crafted to function out-of-the-box with minimal configuration.

## Structure

- **[`pipeline`](pipeline)**: Contains YAML files for GitLab pipelines.
- **[`script`](script)**: Houses common scripts utilized in pipelines.

## How to Use This Repository

To use the pipeline files from this repository, simply include them in your GitLab CI configuration using the `include`
keyword, specifying the appropriate version (tag).

### Example

```yaml
include:
  - project: 'ordinarynick-portfolio/common-ci-pipelines'
    ref: v1.0
    file: 'pipeline/lint/gitlab-ci_file_lint.yaml'
```

## How to Override/Fill Values in Jobs

You can customize the CI jobs by setting variables in your .gitlab-ci.yml file.

Example:

```yaml
Gitlab-CI:Lint:
  variables:
    FILE_NAME: gitlab-ci.yaml
```

## Versions

Versions follow semantic versioning, prefixed with "v" due to GitLab CI include keyword requirements.
